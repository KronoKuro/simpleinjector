﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SimplIngector.Startup))]
namespace SimplIngector
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

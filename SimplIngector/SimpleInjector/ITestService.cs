﻿namespace SimplIngector.SimpleInjector
{
    public interface ITestService
    {
        int TestInjector();
    }
}
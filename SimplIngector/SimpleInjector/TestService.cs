﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimplIngector.SimpleInjector
{
    public class TestService : ITestService
    {
        public int TestInjector()
        {
            return 111;
        }
    }
}